module Main where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, logShow)
import Data.Maybe (Maybe(..), fromJust)
import Graphics.Canvas (CANVAS, Context2D, getCanvasElementById, getContext2D, stroke, beginPath, arc, closePath, moveTo, lineTo)
import Math as Math
import Data.Int (toNumber)
import DOM (DOM)
import DOM.Event.EventTarget (eventListener, addEventListener)
import DOM.HTML.Types(htmlDocumentToParentNode, HTMLElement)
import DOM.HTML.Event.EventTypes as EventTypes
import DOM.Event.Types (Event)
import DOM.HTML (window)
import DOM.HTML.Window (document)
import DOM.Node.ParentNode (querySelector)
import DOM.Node.Types (elementToEventTarget, Element)
import DOM.Node.Element (setAttribute)
import DOM.HTML.HTMLElement (offsetLeft, offsetTop)
import Unsafe.Coerce (unsafeCoerce)
import Data.Nullable (toMaybe)
import DOM.Event.MouseEvent (eventToMouseEvent, clientX, clientY)
import Control.Monad.Except (runExcept)
import Data.Either (Either(..))
import Control.Monad.Eff.Ref (REF, newRef, modifyRef, readRef, Ref)
import Partial.Unsafe (unsafePartial)

foreign import setText :: forall eff. String -> Element -> Eff (dom :: DOM | eff) Unit

width :: Number
width  = 600.0

height :: Number
height = 600.0

blockSize :: Number
blockSize = width / 3.0

type RowColumn = { row     :: Int
                 , column  :: Int
                 }
type Point = { x :: Number
             , y :: Number
             }

drawHollowCircle :: forall e. Context2D -> Number -> Number -> Number -> Eff (canvas :: CANVAS | e) Context2D
drawHollowCircle ctx x y radius = do
  let circleParam = { x: x, y: y, r: radius, start: 0.0, end: Math.pi * 2.0 }
  --setLineWidth 5.0 ctx
  beginPath ctx
  arc ctx circleParam
  stroke ctx
  closePath ctx

drawX :: forall e. Context2D -> Number -> Number -> Eff (canvas :: CANVAS | e) Context2D
drawX ctx centerX centerY = do
  beginPath ctx
  let padding = 30.0 -- 10 pixels of padding
  moveTo ctx (centerX - (blockSize / 2.0) + padding) (centerY - (blockSize / 2.0) + padding)
  lineTo ctx (centerX + (blockSize / 2.0) - padding) (centerY + (blockSize / 2.0) - padding)
  moveTo ctx (centerX + (blockSize / 2.0) - padding) (centerY - (blockSize / 2.0) + padding)
  lineTo ctx (centerX - (blockSize / 2.0) + padding) (centerY + (blockSize / 2.0) - padding)
  stroke ctx
  closePath ctx

drawVerticalLine :: forall e. Context2D -> Number -> Eff (canvas :: CANVAS | e) Context2D
drawVerticalLine ctx x = do
  beginPath ctx
  moveTo ctx x 0.0
  lineTo ctx x height
  stroke ctx
  closePath ctx

drawHorizontalLine:: forall e. Context2D -> Number -> Eff (canvas :: CANVAS | e) Context2D
drawHorizontalLine ctx y = do
  beginPath ctx
  moveTo ctx 0.0 y
  lineTo ctx width y
  stroke ctx
  closePath ctx

elementToHTMLElement :: Element -> HTMLElement
elementToHTMLElement = unsafeCoerce

setupBoard :: forall e. Context2D -> Eff (canvas :: CANVAS | e) Context2D
setupBoard ctx = do
  drawVerticalLine ctx (blockSize)
  drawVerticalLine ctx (2.0 * blockSize)
  drawHorizontalLine ctx (blockSize)
  drawHorizontalLine ctx (2.0 * blockSize)

-- valToRegionNumber :: Number -> Int
-- valToRegionNumber val = case val of
--               _ | val < blockSize                                 -> 0
--               _ | blockSize <= val && val <= (2.0 * blockSize)    -> 1
--               _                                                   -> 2 

valToRegionNumber :: Number -> Int
valToRegionNumber val = if val < blockSize
                          then 0
                          else if blockSize <= val && val <= (2.0 * blockSize)
                          then 1
                          else 2


findRegion :: Number -> Number -> RowColumn
findRegion x y = {column: column, row: row}
  where
    row    = valToRegionNumber y
    column = valToRegionNumber x

regionToCenter :: RowColumn -> Point
regionToCenter regionRecord = {x: centerX, y: centerY}
  where
    centerX = (toNumber regionRecord.column) * blockSize + blockSize / 2.0
    centerY = (toNumber regionRecord.row   ) * blockSize + blockSize / 2.0

changeTurnText :: forall e. String -> String -> Eff (dom :: DOM | e) Unit
changeTurnText selector text = do
  doc          <- window >>= document
  nullableElem <- querySelector selector (htmlDocumentToParentNode doc)
  let elem = unsafePartial $ fromJust $ toMaybe nullableElem
  setText text elem


drawMove :: forall e. Context2D -> Number -> Number -> Ref Boolean -> Eff (canvas :: CANVAS, console :: CONSOLE, dom :: DOM, ref :: REF| e) Context2D
drawMove ctx x y turnRef = do
  let centerCoords = regionToCenter $ findRegion x y
  logShow $ show centerCoords.x <> " " <>  show centerCoords.y
  turn <- readRef turnRef
  modifyRef turnRef (\turn -> not turn)
  if turn
    then do
      changeTurnText "#turnText" "X's turn"
      drawX ctx centerCoords.x centerCoords.y
    else do
      changeTurnText "#turnText" "O's turn"
      drawHollowCircle ctx centerCoords.x centerCoords.y (blockSize/2.0 - 10.0)

onCanvasWithCtx :: forall e. Element -> Context2D -> Ref Boolean -> (Event -> Eff (canvas :: CANVAS, console :: CONSOLE, dom :: DOM, ref::REF | e) Context2D)
onCanvasWithCtx canvas ctx turnRef = (\ev -> case (runExcept $ eventToMouseEvent ev) of
  Right mouseEv -> do
    let htmlCanvasElem = elementToHTMLElement canvas
    offsetL <- offsetLeft htmlCanvasElem
    offsetT <- offsetTop htmlCanvasElem
    drawMove ctx (toNumber (clientX mouseEv) - offsetL) (toNumber (clientY mouseEv) - offsetT) turnRef
  Left  error   -> pure ctx
)

main :: forall e. (Partial) => Eff (console :: CONSOLE, canvas :: CANVAS, dom :: DOM, ref :: REF | e) Unit
main = void $ do
  Just canvas  <- getCanvasElementById "canvas"
  ctx          <- getContext2D canvas
  doc          <- window >>= document -- just makes it easier rather than 2 variables
  nullableElem <- querySelector "#canvas" (htmlDocumentToParentNode doc)
  turnRef      <- newRef true -- true for X, false for O
  let elem     = fromJust $ toMaybe nullableElem
  addEventListener EventTypes.click (eventListener (onCanvasWithCtx elem ctx turnRef)) false (elementToEventTarget elem)
  --clearRect ctx {x: 0.0, y: 0.0, w: 600.0, h: 600.0}
  setupBoard ctx

exports.setText = function(text) {
    return function(element) {
        return function() {
            element.innerText = text;
            return {};
        }
    }
}
